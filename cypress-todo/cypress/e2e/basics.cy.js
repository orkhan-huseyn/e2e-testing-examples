/// <reference types="Cypress"/>

describe('tasks page', () => {
  beforeEach(() => {
    cy.visit('http://127.0.0.1:5173/');
  });

  it('should render the main image', () => {
    cy.get('.main-header').find('img');
  });

  it('should display the page title', () => {
    cy.get('h1').should('have.length', 1);
    cy.get('h1').contains('React Tasks');
  });

  it('should contain add task button', () => {
    cy.get('[data-cy="start-add-task-button"]').contains('Add Task');
  });

  it('should open and close the new task modal', () => {
    cy.contains('Add Task').click();
    cy.get('dialog.modal').should('exist');
    cy.get('.backdrop').click({ force: true });
    cy.get('.backdrop').should('not.exist');
    cy.get('dialog.modal').should('not.exist');

    cy.contains('Add Task').click();
    cy.contains('Cancel').click();
    cy.get('.backdrop').should('not.exist');
    cy.get('dialog.modal').should('not.exist');
  });

  it('should create a new task', () => {
    cy.contains('Add Task').click();

    cy.get('input#title').type('New task');
    cy.get('textarea#summary').type('Some description');

    cy.get('dialog.modal').contains('Add Task').click();
    cy.get('.backdrop').should('not.exist');
    cy.get('dialog.modal').should('not.exist');

    cy.get('.task').should('have.length', 1);
    cy.get('.task').find('h2').contains('New task');
    cy.get('.task').find('p').contains('Some description');
  });

  it('should validate user input', () => {
    cy.contains('Add Task').click();

    cy.get('dialog.modal').contains('Add Task').click();
    cy.get('dialog.modal').contains('Please provide values');
  });

  it('should filter tasks', () => {
    cy.contains('Add Task').click();

    cy.get('input#title').type('New task');
    cy.get('textarea#summary').type('Some description');
    cy.get('select#category').select('urgent');

    cy.get('dialog.modal').contains('Add Task').click();
    cy.get('.task').should('have.length', 1);

    cy.get('select#filter').select('moderate');
    cy.get('.task').should('have.length', 0);

    cy.get('select#filter').select('urgent');
    cy.get('.task').should('have.length', 1);

    cy.get('select#filter').select('all');
    cy.get('.task').should('have.length', 1);
  });

  it('should add multiple tasks', () => {
    cy.contains('Add Task').click();

    cy.get('input#title').type('Task 1');
    cy.get('textarea#summary').type('First task');
    cy.get('select#category').select('urgent');

    cy.get('dialog.modal').contains('Add Task').click();
    cy.get('.task').should('have.length', 1);

    cy.contains('Add Task').click();

    cy.get('input#title').type('Task 2');
    cy.get('textarea#summary').type('Second task');
    cy.get('select#category').select('moderate');

    cy.get('dialog.modal').contains('Add Task').click();
    cy.get('.task').should('have.length', 2);

    cy.get('.task').eq(0).contains('First task');
    cy.get('.task').eq(1).contains('Second task');
  });
});
