/// <reference types="cypress" />

describe('share location', () => {
  beforeEach(() => {
    cy.clock();
    cy.fixture('user-location.json').as('userLocation');
    cy.visit('/').then((window) => {
      cy.get('@userLocation').then((position) => {
        cy.stub(window.navigator.geolocation, 'getCurrentPosition')
          .as('getUserLocation')
          .callsFake((cb) => {
            setTimeout(() => {
              cb(position);
            }, 100);
          });
      });

      cy.stub(window.navigator.clipboard, 'writeText')
        .as('copyToClipboard')
        .resolves();

      cy.spy(window.localStorage, 'getItem').as('getStoredLocation');
      cy.spy(window.localStorage, 'setItem').as('storeLocation');
    });
  });

  it('should fetch the user location', () => {
    cy.get('[data-cy="get-loc-btn"]').click();
    cy.get('@getUserLocation').should('have.been.called');
    cy.get('[data-cy="get-loc-btn"]').should('be.disabled');
    cy.get('[data-cy="actions"]').should('contain', 'Location fetched');
  });

  it('should share a location URL', () => {
    cy.get('[data-cy="name-input"]').type('John Doe');
    cy.get('[data-cy="get-loc-btn"]').click();
    cy.get('[data-cy="share-loc-btn"]').should('not.be.disabled');
    cy.get('[data-cy="share-loc-btn"]').click();
    cy.get('@userLocation').then((position) => {
      const { latitude, longitude } = position.coords;
      cy.get('@copyToClipboard').should(
        'have.been.calledWithMatch',
        new RegExp(`${latitude}.*${longitude}.*${encodeURI('John Doe')}`)
      );
    });
    cy.get('@storeLocation').should('have.been.called');
    cy.get('[data-cy="share-loc-btn"]').click();
    cy.get('@getStoredLocation').should('have.been.called');

    cy.get('[data-cy="info-message"]').should('be.visible');
    cy.tick(2000);
    cy.get('[data-cy="info-message"]').should('not.be.visible');
  });
});
