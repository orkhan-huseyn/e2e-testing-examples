import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    baseUrl: 'http://127.0.0.1:5173',
    setupNodeEvents(on, config) {
      on('task', {
        seedDatabase() {
          console.log('Seeding database...');
          return null;
        },
      });
    },
  },
});
