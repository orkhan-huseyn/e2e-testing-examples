/// <reference types="Cypress" />

describe('page navigation', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should navigate between pages', () => {
    cy.get('[data-cy="header-about-link"]').click();
    cy.get('h1').contains('About Us');
    cy.location('pathname').should('equal', '/about');
    cy.go('back');
    cy.location('pathname').should('equal', '/');

    cy.get('[data-cy="header-about-link"]').click();
    cy.get('[data-cy="header-home-link"]').click();
    cy.location('pathname').should('equal', '/');
  });
});
