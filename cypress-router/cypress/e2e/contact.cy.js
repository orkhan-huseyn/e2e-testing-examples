/// <reference types="Cypress" />

describe('contact form', () => {
  beforeEach(() => {
    cy.visit('/about');
    cy.task('seedDatabase');
  });

  it('should submit the form', () => {
    cy.get('[data-cy="contact-input-message"]').type('Hello, World!');
    cy.get('[data-cy="contact-input-name"]').type('John Doe');

    cy.get('[data-cy="contact-btn-submit"]').as('submitButton');

    cy.get('@submitButton')
      .contains('Send Message')
      .and('not.have.attr', 'disabled');

    cy.get('[data-cy="contact-input-email"]').type('test@example.com{enter}');

    cy.get('@submitButton').contains('Sending...').and('have.attr', 'disabled');
  });

  it('should validate form fields on submit', () => {
    cy.get('[data-cy="contact-btn-submit"]').click();
    cy.get('[data-cy="contact-btn-submit"]').then((el) => {
      expect(el).to.not.have.attr('disabled');
      expect(el.text()).to.not.equal('Sending...');
    });
    cy.get('[data-cy="contact-btn-submit"]')
      .contains('Send Message')
      .and('not.have.attr', 'disabled');
    cy.screenshot();
  });

  it('should validate form fields on blur', () => {
    cy.get('[data-cy="contact-input-message"]').as('messageInput');
    cy.get('@messageInput').focus().blur();
    cy.get('@messageInput')
      .parent()
      .should('have.attr', 'class')
      .and('match', /invalid/);

    cy.get('[data-cy="contact-input-name"]').as('nameInput');
    cy.get('@nameInput').focus().blur();
    cy.get('@nameInput')
      .parent()
      .should('have.attr', 'class')
      .and('match', /invalid/);

    cy.get('[data-cy="contact-input-email"]').as('emailInput');
    cy.get('@emailInput').focus().blur();
    cy.get('@emailInput')
      .parent()
      .should('have.attr', 'class')
      .and('match', /invalid/);
  });
});
